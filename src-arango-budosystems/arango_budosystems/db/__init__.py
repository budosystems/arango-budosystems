from arango import ArangoClient
from arango_orm import Database


def connect_from_settings(settings):
    """

    :param settings:

    :return:
    """
    ar_settings = {k.replace('arango.',''): v for k, v in settings.items() if lambda k: k.startswith('arango.')}

    db = connect(protocol=ar_settings['protocol'],
                 host=ar_settings['host'],
                 port=ar_settings['port'],
                 dbname=ar_settings['db'],
                 username=ar_settings['username'],
                 password=ar_settings['password'])
    return db


def connect(protocol, host, port, dbname, username, password):
    """
    :param protocol:
    :param host:
    :param port:
    :param dbname:
    :param username:
    :param password:

    :return:
    """
    hosts = f'{protocol}://{host}:{port}'
    client = ArangoClient(hosts=hosts)
    db_connection = client.db(dbname, username=username, password=password)

    db = Database(db_connection)
    return db


