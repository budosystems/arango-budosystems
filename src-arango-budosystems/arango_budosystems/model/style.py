from __future__ import annotations
from typing import *

from datetime import timedelta
from collections import OrderedDict

from arango_orm import fields as f
from arango_orm.references import relationship
from arango_orm.graph import GraphConnection

from . import core, contact

entity_classes = {'Studio', 'Style', 'Rank', 'Requirement', 'Skill', 'TimeInClassRequirement',
                  'TimeInRankRequirement', 'AttendanceRequirement', 'GenericRequirement'}
link_classes = {'Rank', 'RankStyle', 'RankRequirement'}
graph_classes = {'CurriculumGraph'}
__all__ = list(entity_classes | link_classes | graph_classes | {'entity_classes', 'link_classes', 'graph_classes'})


class Business(contact.Contactable, core.BasicInfo):
    """Represents the business(es) at the root of the database."""
    __collection__ = 'businesses'


class Studio(contact.Contactable, core.BasicInfo):
    """Represents the studio location(s) where classes are held."""
    __collection__ = 'studios'


class Style(core.BasicInfo):
    """Represents the style of practice (e.g. martial arts style) being taught
    at the studio.
    """
    __collection__ = 'styles'


class Curriculum(core.BasicInfo):
    """Represents the training curriculum for studios that have their students
    progress in rank based on specific requirements.
    """
    __collection__ = 'curricula'

    class Meta:
        verbose_name = 'curriculum'
        verbose_name_plural = 'curricula'

    style_key = f.String()
    style = relationship(Style, "style_key")

    # ordered dictionary of tag: label, used for displaying in ranks and requirements in tabular format
    requirement_schema = f.Dict()

    ranks = relationship(__name__ + ".Rank", "_key", target_field="style_key")

    @property
    def lowest_rank(self):
        """

        :return:
        """
        q = """
            WITH @@collection
            FOR r IN ranks 
                FILTER r._id NOT IN 
                    (FOR p IN progresses_to RETURN p._to)
                AND r._id IN (FOR c IN curriculum_for_rank FILTER c._from == @curriculum RETURN c._to)
                RETURN r
            """

        return self._db.query(Rank).aql(q, bind_vars={'curriculum': self._id})[0]

    @property
    def ranks(self):
        """

        :return:
        """
        q = """
            WITH @@collection
            LET first_rank = (
                FOR r IN ranks 
                FILTER r._id NOT IN 
                    (FOR p IN progresses_to RETURN p._to)
                AND r._id IN (FOR c IN curriculum_for_rank FILTER c._from == @curriculum RETURN c._to)
                RETURN r
            )[0]
            
            FOR rank IN 0..50 OUTBOUND first_rank progresses_to 
            RETURN rank
        """
        return self._db.query(Rank).aql(q, bind_vars={'curriculum': self._id})

    def add_rank(self, rank):
        """
        :param rank:
        """
        pass

    def remove_rank(self, rank):
        """
        :param rank:
        """
        pass

    def clone(self, equivalent_ranks: bool=False) -> Curriculum:
        """Creates a deep copy of this curriculum including it ranks. If
        equivalent_ranks is True, then create an equivalence

        :param equivalent_ranks:
        """
        pass


class Rank(core.BasicInfo):
    """Represents a martial arts rank."""
    __collection__ = 'ranks'


class CurriculumRank(core.Link):
    """Links a Curriculum to a Rank."""
    __collection__ = 'curriculum_for_rank'
    _default_from = Curriculum
    _default_to = Rank
    #_index = [{'type': 'hash', 'fields': ['_to'], 'unique': True}]


class Requirement(core.BasicInfo):
    """Base class for a requirement for a student to meet to earn a rank."""
    __collection__ = "requirements"


class RankRequirement(core.Link):
    """Links a Rank to a Requirement."""
    __collection__ = "rank_requirements"
    _default_from = Rank
    _default_to = Requirement
    tag = f.String()


class NoRequirement(Requirement):
    """Singleton to serve as a placeholder where needed."""
    pass


class Skill(Requirement):
    """Specialized Requirement representing a teachable skill the student can learn."""
    instructions = f.String(required=False, allow_none=True)
    teaching_notes = f.String(required=False, allow_none=True)


class RequirementGroup(Requirement):
    """Specialized Requirement for grouping multiple other Requirement objects under a single entry."""
    # marshmallow bug requires many=True in both spots
    group = f.Nested(Requirement.schema(many=True), many=True, required=True)


class N_OfRequirementGroup(RequirementGroup):
    """RequirementGroup where only a subset of the requirements needs to be met."""
    min_in_group = f.Integer()


class TimeInClassRequirement(Requirement):
    """Specialized Requirement for time in class expectations."""
    min_class_time = f.TimeDelta(precision='seconds', required=True)

    def min_hours(self, min_hours):
        """
        Args:
            min_hours:
        """
        self.min_class_time = timedelta(hours=min_hours)


class TimeInRankRequirement(Requirement):
    """Specialized Requirement for time in rank expectations."""
    min_rank_time = f.TimeDelta(precision='days', required=True)


class AttendanceRequirement(Requirement):
    """Specialized Requirement for minimum attendance expectations."""
    min_attended_classes = f.Integer()


class GenericRequirement(Requirement):
    """Requirement type for cases not represented by another specialization."""
    wording = f.String(required=False, allow_none=True)


class CurriculumGraph(core.Structure):
    """Graph specific to the curriculum."""
    __graph__ = 'curriculum_graph'

    graph_connections = [
        GraphConnection(Studio, core.Link("offers"), Curriculum),
        GraphConnection(Rank, core.Link("progresses_to"), Rank),
        GraphConnection(Curriculum, CurriculumRank, Rank),
        GraphConnection(Rank, RankRequirement, Requirement),
        GraphConnection(Curriculum, core.Link("replaces"), Curriculum),
        GraphConnection(Curriculum, core.Link("progresses_to"), Curriculum),
        GraphConnection(CurriculumRank, core.Link("is_equivalent_to"), CurriculumRank)
    ]
