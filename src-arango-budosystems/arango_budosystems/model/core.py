from __future__ import annotations
from typing import *

import uuid
import re
import unicodedata
from collections import namedtuple

from six import with_metaclass

from marshmallow import Schema

from arango.collection import EdgeCollection
from arango_orm import Collection, Relation, Graph
from arango_orm.collections import CollectionMeta
from arango_orm import fields as f
from arango_orm.event import listens_for

from ..util import *

__all__ = ['Entity', 'Link', 'BasicInfo', 'Structure']


# typing aliases
MappingStrAny = Mapping[str, Any]
OptStr = Optional[str]
SchemaType = Type[Schema]
EntityType = Type['Entity']
LinkList = List['Link']


class EntityMeta(CollectionMeta):
    """Meta class for all Entity classes.
    
    Adds class methods collection and classname to the Entity classes.
    """

    @staticmethod
    def __prepare__(name, bases, **kwargs) -> dict:
        """

        :param name:
        :param bases:
        :param kwargs:

        :return:
        """
        new_attr = dict(
            # collection=property(EntityMeta.collection.fget), # Works, but adds fields to the document
            # classname=property(EntityMeta.classname.fget)
            collection=classmethod(EntityMeta.collection),
            classname=classmethod(EntityMeta.classname)
        )
        return new_attr

    # @property
    def collection(cls) -> OptStr:
        """

        Returns
        -------

        """
        entcls = cls
        if not isinstance(cls, type):
            entcls = type(cls)
        if hasattr(entcls, '__collection__'):
            return entcls.__collection__
        return None

    # @property
    def classname(cls) -> str:
        """
        Convenience method that returns the name of the class.  If called on a class, returns it's own name.
        If called on an instance, returns the name of it's class.

        Returns
        -------
        A string of the name of the class.
        """
        if isinstance(cls, type):
            return cls.__name__
        return type(cls).__name__


class Entity(with_metaclass(EntityMeta, Collection)):
    """
    Base class for all collections in Budo Systems.

    Adds many convenience features to the arango_orm.Collection
    to allow for multiple document types to share the same collection space.
    """

    # UUID type doesn't play well with _id creation code.
    _key = f.String(lambda: str(uuid.uuid1()))
    _allow_extra_fields = True

    _entity_type = f.Dict()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._entity_type = {
            'class': type(self).__qualname__,
            'module': type(self).__module__
        }

    class SchemaMeta:
        """Meta data recognized by the marshmallow Schema framework to be integrated into the automatically
        generated subclass of marshmallow.Schema.  Subclasses of Entity can customize this metadata by implementing
        their own SchemaMeta nested class."""
        ordered = True
        strict = True
        index_errors = True
        # exclude = ['collection', 'classname'] # why?

    class SchemaExtra:
        """Additional properties and methods to be integrated into the automatically
        generated subclass of marshmallow.Schema.  Subclasses of Entity can customize these additions by implementing
        their own SchemaExtra nested class."""
        pass

    @classmethod
    def schema(cls, *args, **kwargs) -> Schema:
        """
        Override Collection.schema to incorporate SchemaMeta and SchemaExtra.

        Schema hierarchy parallels the Entity hierarchy.
        The generated Schema subclass is cached so that subsequent calls are efficient.
        """

        SchemaClass: SchemaType = EntitySchemata(cls)
        return SchemaClass(*args, **kwargs)

    class Meta:
        pass
    #    verbose_name = 'entity'
    #    verbose_name_plural = 'entities'

    @classmethod
    def _pre_load(cls, in_dict: MappingStrAny, only=None, instance=None, db=None) -> Entity:
        """

        :param in_dict:
        :param only:
        :param instance:
        :param db:

        :return:
        """
        if '_entity_type' in in_dict:
            # Find the specific class to represent the data stored in the dictionary
            ent_type: EntityType = class_for_name(in_dict["_entity_type"]["class"], in_dict["_entity_type"]["module"])
            # Remove the type tag - recursion stopper (it should be re-added automatically during construction)
            del in_dict["_entity_type"]
            # Use the _load from the retrieved class to ensure any overrides are taken into account
            ret = ent_type._load(in_dict=in_dict, only=only, instance=instance, db=db)
        else:
            # No type found in the data, use the generic _load from Collection
            ret = super()._load(in_dict=in_dict, only=only, instance=instance, db=db)
        return ret

    @classmethod
    def _load(cls, in_dict: object, only: object = None, instance: object = None, db: object = None) -> object:
        """
        Overrides Collection._load to allow instances of multiple classes to coexist in the same collection.
        Especially useful when subclassing generic types.

        :param in_dict:
        :param only:
        :param instance:
        :param db:

        :return: An instance of this Entity class
        """
        return cls._pre_load(in_dict=in_dict, only=only, instance=instance, db=db)

    def __str__(self) -> str:
        ret = type(self).__qualname__ + " {\n"
        ret += f'  _key = {self._key}\n'
        for fn, ft in self._fields.items():
            if not fn.startswith('_') and ('private' not in ft.metadata or not ft.metadata['private']):
                ret += f'  {fn} = {str(getattr(self, fn))}\n'
        ret += '}\n'
        return ret

    def __getattr__(self, item: str) -> Any:
        if item in ['__provides__', '_pre_process', '_post_process']:
            return lambda *args, **kwargs:  None
        else:
            return super().__getattribute__(item)

    def links(self, link_type: Union[Type[Link], Link, Mapping, str], direction: OptStr=None) -> LinkList:
        """
        Provides a list of `Link` objects connecting this Entity to other Entity objects.

        Parameters
        ----------
        link_type
            An indication of the type of link to retrieve. (Required)
        direction : {'in', 'out', None}
            The direction of the links. Allowed values are "in"
            and "out". If not set, links in both directions are returned.

        Returns
        -------
        List of Link objects
        """
        if isinstance(link_type, Link) or issubclass(link_type, Link):
            link_name = link_type.__collection__
        elif "__collection__" in link_type:
            link_name = link_type["__collection__"]
        elif isinstance(link_type, str):
            link_name = link_type

        edge_collection = EdgeCollection(self._db._conn, self._db._executor, None, link_name)

        raw_edges = edge_collection.edges(self._id, direction)['edges']
        link_list = [Link._load(raw, db=self._db) for raw in raw_edges]

        link: Link
        for link in link_list:
            # bind `self` to the links as an object.
            if link._to == self._id:
                link._object_to = self
            if link._from == self._id:
                link._object_from = self

        return link_list

    @property
    def keyslug(self) -> str:
        """
        A convenience method to get either the key or the slug (defined in `BasicInfo`) of the Entity object.

        Returns
        -------
        The slug value if it exists, the _key value otherwise.
        """
        if hasattr(self, 'slug'):
            keyslug = self.slug
        elif hasattr(self, '_key'):
            keyslug = self._key
        else:
            raise ValueError("Missing _key and slug attributes")
        return keyslug


StrSeq = Union[str, List[str], Tuple[str, ...], None]


class Link(Entity, Relation):
    """
    Base class for all edge collections in Budo Systems.
    """
    _default_from: ClassVar[Optional[str]] = None
    _default_to: ClassVar[Optional[str]] = None

    EndPoints = namedtuple("EndPoints", ['from_entity', 'to_entity'])

    def __init__(self,
                 collection_name: Optional[str] = None,
                 _collections_from: StrSeq = None,
                 _collections_to: StrSeq = None,
                 **kwargs):
        """

        :param collection_name:
        :param _collections_from:
        :param _collections_to:
        :param kwargs:
        """
        if _collections_from is None:
            _collections_from = self._default_from

        if _collections_to is None:
            _collections_to = self._default_to

        self._endpoints = None

        super().__init__(collection_name=collection_name,
                         _collections_from=_collections_from,
                         _collections_to=_collections_to,
                         **kwargs)

    @classmethod
    def _load(cls, in_dict: MappingStrAny, only: object = None, instance: object = None, db: object = None) -> object:
        """
        Overrides Relation._load to allow instances of multiple classes to coexist in the same collection.
        Especially useful when subclassing generic types.

        :param in_dict:
        :param only:
        :param instance:
        :param db:
        :return: An instance of Link populated with database data.
        """
        new_inst = super()._load(in_dict=in_dict, only=only, instance=instance, db=db)
        ret = cls._pre_load(in_dict=in_dict, only=only, instance=new_inst, db=db)

        return ret

    def __str__(self) -> str:
        ret = type(self).__qualname__ + " {\n"
        ret += f'  _key = {self._key}\n'
        ret += f'  _from = {self._from}\n'
        ret += f'  _to = {self._to}\n'
        for fn, ft in self._fields.items():
            if not fn.startswith('_') and ('private' not in ft.metadata or not ft.metadata['private']):
                ret += f'  {fn} = {str(getattr(self, fn))}\n'
        ret += '}\n'
        return ret

    def __setattr__(self, key, value) -> None:
        super().__setattr__(key, value)
        if key in ['_from', '_to']:
            self._dirty.add(key)

    @property
    def from_entity(self) -> Entity:
        """

        :return:
        """
        if not self._object_from or '_from' in self._dirty:
            self._object_from = Entity._load(self._db.document(self._from), db=self._db)
        return self._object_from

    @property
    def to_entity(self) -> Entity:
        """

        :return:
        """
        if not self._object_to or '_to' in self._dirty:
            self._object_to = Entity._load(self._db.document(self._to), db=self._db)
        return self._object_to

    @property
    def endpoints(self) -> EndPoints:
        """

        :return:
        """
        if not self._endpoints or '_from' in self._dirty or '_to' in self._dirty:
            self._endpoints = self.EndPoints(self.from_entity, self.to_entity)
        return self._endpoints


class BasicInfo(Entity):
    """
    Abstract class for fields that will be common among most concrete classes.

    :attr name:
    :attr description:
    :attr slug:
    """
    _index = [{'type': 'hash', 'fields': ['slug'], 'unique': True}]
    name = f.String(required=True)
    description = f.String(required=False, allow_none=True)
    slug = f.String(required=True)


@listens_for(BasicInfo, 'pre_add')
def slugify_name(entity, _event, *_args, **_kwargs) -> None:
    """
    If the slug is not already set with a non-empty string, assign it a normalized version of the name
    field.

    :param entity: An instance of BasicInfo.
    :param _event: (not used - provided by event protocol)
    :param _args: (not used - provided by event protocol)
    :param _kwargs: (not used - provided by event protocol)
    """
    if not hasattr(entity, 'slug') or not entity.slug:
        entity.slug = re.sub(r'\W', r'_', unicodedata.normalize('NFD', entity.name.casefold().strip()))


class Structure(Graph):
    """
    Reserved name for future extensions to the Graph class.
    """
    pass


class EntitySchemata(ParallelFactory[Entity, Schema], source_base=Entity, para_base=Schema, suffix="Schema"):
    """
    Factory class for :class:`Schema` classes related to :class:`Entity` classes.

    .. rubric:: Usage

    :samp:`EntitySchemata({E})` (where :samp:`issubclass({E}, Entity)` is ``True``) will return the :class:`Schema` subclass
    corresponding to :var:`E`.
    If the Schema subclass already exists and is cached, that subclass is returned.  Otherwise, one will be
    generated based on the structure of the given subclass of :class:`Entity` :var:`E`, then cached and returned.
    The structure of the of the Schema class tree will parallel that of the Entity class tree.
    """

    @classmethod
    def build_parallel_class(cls, data_class: Type[Entity], p_name: str, extra_attrs: MappingStrAny={}) -> Type[Entity]:
        """

        :param data_class:
        :param p_name:
        :param extra_attrs:
        :return:
        """
        schema_def = data_class._fields.copy()
        schema_def['Meta'] = data_class.SchemaMeta

        for attr, val in data_class.SchemaExtra.__dict__.items():
            schema_def[attr] = val

        SchemaClass = super().build_parallel_class(data_class, p_name, {**extra_attrs, **schema_def})

        cls.register(SchemaClass)

        return SchemaClass

