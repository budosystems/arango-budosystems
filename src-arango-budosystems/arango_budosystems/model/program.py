from __future__ import annotations
from typing import *

from arango_orm import fields as f
from . import core


class MembershipOption(core.BasicInfo):
    """
    Represents an membership offering, used as a template for a membership agreement.
    """
    __collection__ = "membership_options"

    # General information
    # Inherited: name, description, slug
    duration = f.TimeDelta()
    membership_category = f.String()
    limit_on_classes = f.Field()  # Todo: Figure out proper type of field
    auto_renewal = f.Boolean()

    # Billing information
    installment_plan = f.String()  # Todo: shortlist of payment frequencies
    number_of_installments = f.Integer()
    signup_fee = f.Decimal()
    installment_amount = f.Decimal()
    add_service_tax = f.Boolean()
    add_retail_tax = f.Boolean()
    income_category = f.Field()

    # Programs & Classes
    # Todo: reference list of programs and training classes students can participate in under this option

    active = f.Boolean()
