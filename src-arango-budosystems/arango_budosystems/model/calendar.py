from __future__ import annotations
from typing import *

from arango_budosystems.model import core, style, people
from datetime import date, time, datetime, timedelta

from arango_orm import fields as f
from icalendar import cal, prop
from dateutil import rrule

MappingStrAny = Mapping[str, Any]


class Calendar(core.Entity):
    """Represents a calendar."""
    __collection__ = 'calendars'

    @classmethod
    def from_ical(cls, icaldata: str, multiple: bool = False) -> Calendar:
        """

        :param icaldata:
        :param multiple:
        """
        # Convert iCal string to a python data structure
        ical = cal.Calendar.from_ical(icaldata, multiple)

        # Process the data structure into our model to store in the database
        pass

    def to_ical(self, sorted: bool = True) -> str:
        """

        :param sorted:
        """
        # Convert our model into the icalendar library data structure.
        ical = cal.Calendar()
        # TODO: The rest of this algorithm

        # Convert the result into a string
        return ical.to_ical(sorted)


class TrainingClass(core.BasicInfo):
    """Represents an individual time slot for a class."""
    __collection__ = 'training_classes'

    training_date: date = f.Date()
    start_time: time = f.Time()
    duration: timedelta = f.TimeDelta()


class ClassSchedule(core.BasicInfo):
    """Represents a class schedule."""
    __collection__ = 'class_schedule'

    _rruleset_obj: rrule.rruleset

    template = f.Nested(TrainingClass.schema())  # TODO: Nest, inherit, or link?
    recurrence = f.String()  # rrule / rruleset

    @property
    def rrule(self) -> str:
        return str(self._rruleset_obj)

    @rrule.setter
    def rrule(self, rrstr: str):
        self._rruleset_obj = rrule.rrulestr(rrstr)

    def _pre_load(cls, in_dict: MappingStrAny, only=None, instance=None, db=None) -> core.Entity:
        obj = super()._pre_load(in_dict=in_dict, only=only, instance=instance, db=db)
        return obj

    def _dump(self, only=None, **kwargs):
        super()._dump(only=only, **kwargs)


class TrainingClassSchedule(core.Link):
    __collection__ = 'class_instance_of_schedule'
    _default_from = TrainingClass
    _default_to = ClassSchedule
    _index = [{'type': 'hash', 'fields': ['_from'], 'unique': True}]  # At most 1 instance per schedule


class Attendance(core.Link):
    """An individual attendance record entry for a student."""
    __collection__ = 'attendance'
    _default_from = people.Person
    _default_to = TrainingClass

    late: timedelta = f.TimeDelta(default=0)
    left_early: timedelta = f.TimeDelta(default=0)


class ClassCurriculum(core.Link):
    """Links a TrainingClass to a Curriculum being taught in that class."""
    __collection__ = 'class_curricula'
    _default_from = TrainingClass
    _default_to = style.Curriculum
