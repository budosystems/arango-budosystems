"""
Provides the ArangoDB model classes for Budo Systems.
"""
__all__ = ['core', 'calendar', 'contact', 'people', 'program', 'retail', 'style']