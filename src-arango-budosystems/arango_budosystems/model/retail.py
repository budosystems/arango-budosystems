from __future__ import annotations
from typing import *

from arango_orm import fields as f

from arango_budosystems.model import core


class Product(core.BasicInfo):
    """
    Represents a product for sale.
    """
    __collection__ = 'retail_products'
    sale_price = f.Decimal()
    purchase_cost = f.Decimal()


class ProductVariant(core.BasicInfo):
    """Represents a variation on a product in the catalog."""
    pass


class Bundle(core.BasicInfo):
    """Represents a collection of products to be sold together, possible with a discount."""
    __collection__ = 'retail_bundles'
