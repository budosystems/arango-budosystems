from __future__ import annotations
from typing import *

from datetime import date, datetime, timedelta

from arango_orm import fields as f
from arango_orm.graph import GraphConnection

from . import core, style, contact


entity_classes = {'Person', 'Household'}
link_classes = {'FamilyMember', 'StudentRank', 'StudentRequirement'}
graph_classes = {'PeopleGraph'}

__all__ = list(entity_classes | link_classes | graph_classes | {'entity_classes', 'link_classes', 'graph_classes'})


class UserAccount(core.Entity):
    """
    Represents the login profile and credentials of a system user.
    """

    __collection__ = "user_account"
    username = f.String()
    password = f.String(private=True)


class Person(contact.Contactable):
    __collection__ = "people"

    class Meta:
        verbose_name = 'person'
        verbose_name_plural = 'people'

    # Personal info
    title = f.String(required=False, allow_none=True)
    first_name = f.String(required=True)
    middle_name = f.String(required=False, allow_none=True)
    last_name = f.String(required=True)
    suffix = f.String(required=False, allow_none=True)
    date_of_birth: date = f.Date(required=False, allow_none=True)
    gender = f.String(required=False, allow_none=True)

    # Stats
    start_date: date = f.Date(required=False, allow_none=True)
#    create_date: datetime = f.DateTime(required=True, allow_none=False, default=datetime.now)  # Do we care?

    def __init__(self, **kwargs):
        """
        Args:
            **kwargs:
        """
        super().__init__(**kwargs)
        self._name_fields = None

    @property
    def age_delta(self) -> Optional[timedelta]:
        """
        Calculates the age as a :class:`datetime.timedelta`

        :return: The age as a
        """
        if self.date_of_birth:
            return date.today() - self.date_of_birth
        return None

    @property
    def age_years(self) -> Optional[int]:
        """
        Calculates the age in years and returns the result.
        :return: The age in years of the person, or None if date of birth is not set.
        """
        if self.date_of_birth:
            today = date.today()
            years = today.year - self.date_of_birth.year
            if today.month == self.date_of_birth.month:
                if today.day < self.date_of_birth.day:
                    years -= 1
            elif today.month < self.date_of_birth.month:
                years -= 1
            return years
        return None

    @property
    def name_fields(self) -> List[str]:
        """

        :return: List of fields used to compose the name.
        """
        if not hasattr(self, '_name_fields') or not self._name_fields:
            self._name_fields = ['title', 'first_name', 'middle_name', 'last_name', 'suffix']
        return self._name_fields

    @property
    def names_list(self) -> List[str]:
        """

        :return: List of name components.
        """
        return [getattr(self, nf) for nf in self.name_fields]

    @property
    def ranks(self) -> List:
        """
        .. warning:: Not yet implemented.
        :return: The rank history of this student.
        """
        raise NotImplementedError()


class UserPerson(core.Link):
    """
    Pairs a UserAccount to a Person.
    """
    __collection__ = 'user_person'
    _default_from = UserAccount
    _default_to = Person
    _index = [{'type': 'hash', 'fields': ['_from'], 'unique': True},  # At most 1 person per user
              {'type': 'hash', 'fields': ['_to'], 'unique': True},    # At most 1 user per person
              ]


class Household(contact.Contactable):
    """
    Represents a household.  Contact information shared by everyone in the household should be stored
    here to reduce redundant information.
    """
    __collection__ = "households"


class FamilyMember(core.Link):
    """
    Identifies a Person as a family member of a Household.
    """
    __collection__ = "family_members"
    _default_from = Person
    _default_to = Household
    is_primary_contact = f.Boolean(default=False)


class StudentRank(core.Link):
    """
    Rank a student (Person) has earned.
    """
    __collection__ = "student_rank"
    _default_from = Person
    _default_to = style.CurriculumRank
    date_earned = f.Date()


class StudentRequirement(core.Link):
    """
    Maps a student to a requirement for a given rank.
    """
    __collection__ = 'student_requirements'
    _default_from = Person
    _default_to = style.RankRequirement
    satisfied = f.Boolean(default=False)
    date_satisfied = f.Date(required=False, allow_none=True)


class PeopleGraph(core.Structure):
    """
    Graph of Person related data.
    """
    __graph__ = "people_graph"

    graph_connections = [
        GraphConnection(Person, FamilyMember, Household),
        GraphConnection(Person, core.Link('referred'), Person),
        GraphConnection(Person, StudentRank, style.CurriculumRank),
        GraphConnection(Person, StudentRequirement, style.RankRequirement),
        GraphConnection(UserAccount, UserPerson, Person),
    ]
