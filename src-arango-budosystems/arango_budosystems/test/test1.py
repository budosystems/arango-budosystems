from arango import ArangoClient
from arango_orm import Database
from arango_budosystems.model import core, style, people

from datetime import date

setup = True
cleanup = False

if __name__ == "__main__":

    client = ArangoClient(protocol='http', host='localhost', port=8529)
    test_db_connection = client.db('test', username='test', password='test')

    test_db = Database(test_db_connection)

    collection_classes = [
        style.Studio,
        style.Style,
        style.Rank,
        style.Requirement,
        style.RankStyle,
        people.Person,
        people.Household,
        people.FamilyMember
    ]

    for cls in collection_classes:
        if test_db.has_collection(cls):
            print(f'{cls} already in {test_db}, dropping it')
            test_db.drop_collection(cls)
        print(f'Adding collection for class {cls} to {test_db}')
        test_db.create_collection(cls, edge=issubclass(cls, core.Link))

#    for col in test_db.collections():
#        print(f'Collection {col} is in {test_db}')
    print("=========== Adding objects ===========")
    req = core.Skill()
    req.name = 'Kihòñ Ippón'
    req.description = 'Basic Kata #1'
    req.instructions = '5 steps forward, middle punch'
    req.teaching_notes = 'something insightful'
    test_db.add(req)

    req = core.TimeInClassRequirement()
    req.name = "5 Hours"
    req.description = ''
    req.min_hours(5)
    req.slug = '5-hour-minimum'
    test_db.add(req)

    p = people.Person()
    p.first_name = "Joël"
    p.middle_name = "Paul"
    p.last_name = "Larose"
    p.date_of_birth = date(1977, 11, 12)
    #p.account = people.UserAccount(username="jplarose", password="something")
    test_db.add(p)
    print(f"p._id = {p._id}")

    h = people.Household()
    h.name = "Larose-Moore Family"
    test_db.add(h)
    print(f"h._id = {h._id}")

    f = people.FamilyMember()
    f._from = p._id
    f._to = h._id
    print(f)
    test_db.add(f)
    print(f)

    print()
    print("=========== Retrieving objects ===========")
    for r in test_db.query(core.Requirement).all():
        print(type(r))
        print(str(r))
        print(r.slug)
        print()

    for i in test_db.query(people.Person).all():
        print(type(i))
        print(str(i))
        age = i.age_years()
        print(f"Age: {age if age else 'Unspecified'}")
        print()

    for i in test_db.query(people.Household).all():
        print(type(i))
        print(str(i))
        print()

    for i in test_db.query(people.FamilyMember).all():
        print(type(i))
        print(str(i))
        print()


    if cleanup:
        for cls in collection_classes:
            print(f'Removing {cls} from {test_db}')
            test_db.drop_collection(cls)


