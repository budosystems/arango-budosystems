import unittest
from arango import ArangoClient
from arango_orm import Database

from arango_budosystems.model.people import *


def setUpModule():
    global test_db, client, test_db_connection
    client = ArangoClient(protocol='http', host='localhost', port=8529)
    test_db_connection = client.db('test', username='test', password='test')

    test_db = Database(test_db_connection)


def tearDownModule():
    # Anything to do here to close the connection?
    global test_db, client, test_db_connection
    del test_db


class PersonTest(unittest.TestCase):
    def setUp(self):
        test_db.create_collection(Person)

    def tearDown(self):
        test_db.drop_collection(Person)

    def test_collection_exists(self):
        self.assertTrue(test_db.has_collection(Person))

    def test_add_person(self):
        p = Person()
        p.first_name = "JoÃ«l"
        p.middle_name = "Paul"
        p.last_name = "Larose"

        test_db.add(p)
        self.assertIsNotNone(p._id, "p._id is None")
        self.assertIsNotNone(p._key, "p._key is None")


class HouseholdTest(unittest.TestCase):
    def setUp(self):
        test_db.create_collection(Household)

    def tearDown(self):
        test_db.drop_collection(Household)

    def test_add_household(self):
        h = Household()
        h.name = "Larose-Moore Family"
        test_db.add(h)
        self.assertIsNotNone(h._id, "h._id is None")
        self.assertIsNotNone(h._key, "h._key is None")


class FamilyMemberTest(unittest.TestCase):
    def setUp(self):
        test_db.create_collection(Person)
        test_db.create_collection(Household)
        test_db.create_collection(FamilyMember)

    def tearDown(self):
        test_db.drop_collection(FamilyMember)
        test_db.drop_collection(Household)
        test_db.drop_collection(Person)

    def test_add_family_member(self):
        p = Person()
        p.first_name = "JoÃ«l"
        p.middle_name = "Paul"
        p.last_name = "Larose"
        test_db.add(p)

        h = Household()
        h.name = "Larose-Moore Family"
        test_db.add(h)

        f = FamilyMember()
        f._from = p._id
        f._to = h._id
        test_db.add(f)

        self.assertIsNotNone(f._id, "f._id is None")
        self.assertIsNotNone(f._key, "f._key is None")






if __name__ == "__main__":
    unittest.main()