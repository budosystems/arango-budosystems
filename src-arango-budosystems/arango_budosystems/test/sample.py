import inspect
import datetime as dt
from arango_orm.graph import Graph, GraphConnection

from arango_budosystems.db import connect
from arango_budosystems import model
from arango_budosystems.model import *


# noinspection PyProtectedMember
def main():
    db = connect('http', 'localhost', '8529', 'budosystems_sample', 'sample', 'sample')

    # get all the modules under arango_budosystems.model
    for mdl_name, mdl in inspect.getmembers(model, lambda x: inspect.ismodule(x)):
        # get all the classes in the module
        for cls_name, cls in inspect.getmembers(mdl, lambda x: inspect.isclass(x)):
            if issubclass(cls, core.Entity) and hasattr(cls, '__collection__') and cls.__collection__:
                if db.has_collection(cls):
                    db.collection(cls.__collection__).truncate()
                    #db.drop_collection(cls)
                else:
                    db.create_collection(cls, edge=issubclass(cls, core.Link))
            if issubclass(cls, Graph) and cls.__graph__:
                if db.has_graph(cls.__graph__):
                    db.update_graph(cls(connection=db))
                    #db.drop_graph(cls,False)
                else:
                    db.create_graph(cls())

    larosekarate = style.Studio(name='Larose Karate')
    larosekarate.addresses[...] = contact.PhysicalAddress(label='dojo',
                                                          street='700 Glen Forrest Blvd',
                                                          city='Waterloo',
                                                          province='ON',
                                                          postal_code='N2L 4K6',
                                                          country='Canada')
    larosekarate.addresses[...] = contact.PhysicalAddress(label='mailing',
                                                          street='82 West Acres Cres',
                                                          city='Kitchener',
                                                          province='ON',
                                                          postal_code='N2N 3G8',
                                                          country='Canada')
    larosekarate.emails[...] = contact.EmailAddress(label='sensei', value='sensei@larosekarate.com')
    larosekarate.emails[...] = contact.EmailAddress(label='store', value='store@larosekarate.com')
    larosekarate.emails[...] = contact.EmailAddress(label='info', value='info@larosekarate.com')
    larosekarate.phones[...] = contact.PhoneNumber(label='sensei', value='519-897-9980', can_text=True)

    db.add(larosekarate)

    shorinkan = style.Style(name="Shorin-Ryu Shorinkan", slug="shorinkan")
    shorinkan.description = "Shorinkan is a association in the style of Shorin-Ryu, founded by Nakazato Shugoro."

    db.add(shorinkan)

    kobudo_style = style.Style(name="Kokusairengokai Okinawan Kobudo", slug='kokusairengokai')
    kobudo_style.description = "The Okinawan Kobudo weapons counterpart to Shorinkan Karate."

    db.add(kobudo_style)


    curriculum_data = {
        'karate': {
            'name': "Regular Karate Program",
            'slug':'karate2018',
            'description':
                "Regular Karate program offered by Larose Karate for the Shorinkan style of Karate. (Rev.2018)",
            'style_key': shorinkan._key,
            'requirement_schema': {
                'red': 'Red Stripe',
                'yellow': 'Yellow Stripe',
                'blue': 'Blue Stripe',
                'green': 'Green Stripe',
                'class_time': 'Min Time in Class',
                'teach_time': 'Min Time Teaching',
                'other': 'Other',
            },
        },

        'dragon': {
            'name': "Lil' Dragons",
            'slug':'dragons2018',
            'description': "Karate Program specifically designed for kids ages 4 to 7. (Rev.2018)",
            'style_key': shorinkan._key,
            'requirement_schema': {
                'red': 'Red Stripe',
                'yellow': 'Yellow Stripe',
                'blue': 'Blue Stripe',
                'class_time': 'Min Time in Class',
                'other': 'Other',
            },
        },

        'kobudo': {
            'name': "Kobudo Program",
            'slug': 'kobudo2019',
            'description': "Okinawan Kobudo weapons program (Rev.2019)",
            'style_key': kobudo_style._key,
            'requirement_schema': {
                'red': 'Red Stripe',
                'yellow': 'Yellow Stripe',
                'class_time': 'Min Time in Class',
                'other': 'Other',
            },
        },
    }

    curricula = dict()
    for key, data in curriculum_data.items():
        curricula[key] = style.Curriculum(**data)
        db.add(curricula[key])
        lnk = core.Link("offers", _from=larosekarate._id, _to=curricula[key]._id)
        db.add(lnk)

    ranks = dict()
    c_ranks = {'karate': dict(), 'kobudo': dict(), 'dragon': dict()}
    rank_colours = ['white', 'gold', 'orange', 'purple', 'blue', 'green', 'brown']
    SUFFIXES = {1: 'st', 2: 'nd', 3: 'rd'}
    bb_rank = ['shodan', 'nidan', 'sandan', 'yondan', 'godan', 'rokudan', 'nanadan', 'hachidan', 'kyudan', 'judan']

    def ordinal(num):
        # I'm checking for 10-20 because those are the digits that
        # don't follow the normal counting scheme.
        if 10 <= num % 100 <= 20:
            suffix = 'th'
        else:
            # the second parameter is a default.
            suffix = SUFFIXES.get(num % 10, 'th')
        return str(num) + suffix


    prev_rank = None
    # Lil' Dragons Belts
    for rc in rank_colours + ["black"]:
        rank_name = rc.title()
        rank_slug = rc
        rank_name = "Lil' " + rank_name
        rank_slug = "lil_" + rank_slug
        rank_name += " Belt"
        rank = style.Rank(name=rank_name, slug=rank_slug)
        db.add(rank)
        if prev_rank:
            db.add(core.Link('progresses_to', _from=prev_rank._id, _to=rank._id))
        ranks[rank_slug] = rank
        c_ranks['dragon'][rank_slug] = style.CurriculumRank(_from=curricula['dragon']._id, _to=rank._id)
        db.add(c_ranks['dragon'][rank_slug])
        prev_rank = rank

    # Regular Program and Kobudo Belts
    for style_slug in ['karate', 'kobudo']:
        prev_rank = None
        for rc in rank_colours:
            for striped in ['solid', 'black']:
                rank_name = rc.title()
                rank_slug = rc
                if striped == 'black':
                    rank_name += "/Black"
                    rank_slug += "-black"
                rank_name += " Belt"
                rank = style.Rank(name=rank_name, slug=style_slug+"_"+rank_slug)
                db.add(rank)
                if prev_rank:
                    db.add(core.Link('progresses_to', _from=prev_rank._id, _to=rank._id))
                ranks[rank_slug] = rank
                c_ranks[style_slug][rank_slug] = style.CurriculumRank(_from=curricula[style_slug]._id, _to=rank._id)
                db.add(c_ranks[style_slug][rank_slug])
                prev_rank = rank

        for i, bbr in enumerate(bb_rank):
            rank_name = bbr.title()
            rank_slug = bbr
            rank = style.Rank(name=rank_name, slug=style_slug+"_"+rank_slug,
                              description=ordinal(i+1) + " degree Black Belt")
            db.add(rank)
            if prev_rank:
                db.add(core.Link('progresses_to', _from=prev_rank._id, _to=rank._id))
            c_ranks[style_slug][rank_slug] = style.CurriculumRank(_from=curricula[style_slug]._id, _to=rank._id)
            db.add(c_ranks[style_slug][rank_slug])
            prev_rank = rank

        db.add(core.Link('is_equivalent_to', _from=c_ranks['dragon']['lil_black']._id, _to=c_ranks['karate']['gold-black']._id))
        db.add(core.Link('progresses_to', _from=curricula['dragon']._id, _to=curricula['karate']._id))

    skill_names = {
        'kihon_uke': {
            'name':'Kihon Uke',
            'description': 'Basic Blocks',
            'instructions': "Left, then Right for each block: High Block, Inside Block, Outside Block, \
            Low Block, Knife Hand Block."
        },
        'kihon_geri': {
            'name': 'Kihon Geri',
            'description': 'Basic Kicks',
            'instructions': "Right, then Left for each kick: Front Kick, Side Kick, Roundhouse Kick, Back Kick"
        },
        'kihon_dachi': {
            'name':'Kihon Dachi',
            'description': 'Basic Stances',
        },
        'kihon_uchi': 'Kihon Uchi',
        'kihon_1': 'Kihon Ippon',
        'kihon_2': 'Kihon Nihon',
        'kihon_3': 'Kihon Sambon',
        'fukyu_1': 'Fukyu Dai Ichi',
        'fukyu_2': 'Fukyu Dai Ni',
        'fukyu_3': 'Fukyu Dai San',
        'fukyu_kata': 'Fukyu No Kata',
        'taikyoku_1': 'Taikyoku Shodan',
        'taikyoku_2': 'Taikyoku Nidan',
        'taikyoku_3': 'Taikyoku Sandan',
        'taikyoku_4': 'Taikyoku Yondan',
        'taikyoku_5': 'Taikyoku Godan',
        'nihanchi_1': 'Nihanchi Shodan',
        'nihanchi_2': 'Nihanchi Nidan',
        'nihanchi_3': 'Nihanchi Sandan',
        'pinan_1': 'Pinan Shodan',
        'pinan_2': 'Pinan Nidan',
        'pinan_3': 'Pinan Sandan',
        'pinan_4': 'Pinan Yondan',
        'pinan_5': 'Pinan Godan',
        'uke_waza_1': 'Uke Waza Dai Ichi',
        'uke_waza_2': 'Uke Waza Dai Ni',
        'uke_waza_3': 'Uke Waza Dai San',
        'uke_waza_4': 'Uke Waza Dai Shi',
        'empi_waza': 'Empi Waza',
        'shuto_waza': 'Shuto Waza',
        'yotsukado_01': 'Yotsukado No Renshu Ichibanme',
        'yotsukado_02': 'Yotsukado No Renshu Nibanme',
        'yotsukado_03': 'Yotsukado No Renshu Sambanme',
        'yotsukado_04': 'Yotsukado No Renshu Yonbanme',
        'yotsukado_05': 'Yotsukado No Renshu Gobanme',
        'yotsukado_06': 'Yotsukado No Renshu Rokubanme',
        'yotsukado_07': 'Yotsukado No Renshu Nanabanme',
        'yotsukado_08': 'Yotsukado No Renshu Hachibanme',
        'yotsukado_09': 'Yotsukado No Renshu Kyubanme',
        'yotsukado_10': 'Yotsukado No Renshu Jubanme',
        'yotsukado_11': 'Yotsukado No Renshu Juichibanme',
        'yotsukado_12': 'Yotsukado No Renshu Junibanme',
        'yotsukado_13': 'Yotsukado No Renshu Jusambanme',
        'yotsukado_14': 'Yotsukado No Renshu Juyonbanme',
        'yotsukado_15': 'Yotsukado No Renshu Jugobanme',
        'yotsukado_16': 'Yotsukado No Renshu Jurokubanme',
        'yotsukado_17': 'Yotsukado No Renshu Junanabanme',
        'yotsukado_18': 'Yotsukado No Renshu Juhachibanme',
        'yotsukado_19': 'Yotsukado No Renshu Jukyubanme',
        'yotsukado_20': 'Yotsukado No Renshu Nijubanme',
        'bo_taiso': 'Bo Taiso No Renshu',
        'nunchaku_taiso': 'Nunchaku Taiso No Renshu',
        'tonfa_taiso': 'Tonfa Taiso No Renshu',
        'sai_taiso': 'Sai Taiso No Renshu',
        'eku_taiso': 'Eku Taiso No Renshu',
        'kama_taiso': 'Kama Taiso No Renshu',
        'minoru_yak_1': 'Minoru Yakusoku Kumite Dai Ichi',
        'minoru_yak_2': 'Minoru Yakusoku Kumite Dai Ni',
        'minoru_yak_3': 'Minoru Yakusoku Kumite Dai San',
        'minoru_yak_4': 'Minoru Yakusoku Kumite Dai Shi',
        'minoru_yak_5': 'Minoru Yakusoku Kumite Dai Go',
        'minoru_yak_6': 'Minoru Yakusoku Kumite Dai Roku',
        'minoru_yak_7': 'Minoru Yakusoku Kumite Dai Nana',
        'shugoro_yak_1': 'Shugoro Yakusoku Kumite Dai Ichi',
        'shugoro_yak_2': 'Shugoro Yakusoku Kumite Dai Ni',
        'shugoro_yak_3': 'Shugoro Yakusoku Kumite Dai San',
        'shugoro_yak_4': 'Shugoro Yakusoku Kumite Dai Shi',
        'shugoro_yak_5': 'Shugoro Yakusoku Kumite Dai Go',
        'shugoro_yak_6': 'Shugoro Yakusoku Kumite Dai Roku',
        'shugoro_yak_7': 'Shugoro Yakusoku Kumite Dai Nana',
        'passai_sho': 'Passai Sho',
        'passai_dai': 'Passai Dai',
        'kusanku_sho': 'Kusanku Sho',
        'kusanku_dai': 'Kusanku Dai',
        'chinto': 'Chinto',
        'gorin': 'Gorin No Kata',
        'gojushiho': 'Gojushiho',
        'bo_bo_1': 'Bo-Bo Kumite Dai Ichi',
        'bo_bo_2': 'Bo-Bo Kumite Dai Ni',
        'bo_bo_3': 'Bo-Bo Kumite Dai San',
        'bo_bo_4': 'Bo-Bo Kumite Dai Shi',
        'bo_bo_5': 'Bo-Bo Kumite Dai Go',
        'shushi_1': 'Shushi No Kun',
        'shushi_2': 'Shushi No Kun Sho',
        'shushi_3': 'Shushi No Kun Dai',
        'kubo': 'Kubo No Kun',
        'sakugawa_1': 'Sakukawa No Kun',
        'sakugawa_2': 'Sakukawa Dai Ni',
        'sakugawa_3': 'Sakukawa Dai San',
        'tonfa_1': 'Shorinkan No Tonfa',
        'tonfa_2': 'Shorinkan No Tonfa Dai Ni',
        'nunchaku_1': 'Shorinkan No Nunchaku Dai Ichi',
        'nunchaku_2': 'Shorinkan No Nunchaku Dai Ni',
        'sai_1': 'Sai Dai Ichi',
        'sai_2': 'Sai Dai Ni',
        'sai_3': 'Sai Dai San',
        'kama_1': 'Kama Dai Ichi',
        'kama_2': 'Kama Dai Ni',
        'eku_1': 'Eku Dai Ichi',
        'bo_sai_1': 'Bo-Sai Kumite Dai Ichi',
        'bo_sai_2': 'Bo-Sai Kumite Dai Ni',
        'bo_sai_3': 'Bo-Sai Kumite Dai San',
        'bo_kama_1': 'Bo-Kama Kumite Dai Ichi',
        'bo_kama_2': 'Bo-Kama Kumite Dai Ni',
        'bo_kama_3': 'Bo-Kama Kumite Dai San',
        'bo_tonfa_1': 'Bo-Tonfa Kumite Dai Ichi',
        'bo_tonfa_2': 'Bo-Tonfa Kumite Dai Ni',
        'bo_tonfa_3': 'Bo-Tonfa Kumite Dai San',
    }

    skills = {}
    for slug, details in skill_names.items():
        if isinstance(details, str):
            skills[slug] = style.Skill(name=details, slug=slug)
        else:
            skills[slug] = style.Skill(slug=slug, **details)
        db.add(skills[slug])

    skills['project'] = style.Requirement

    skill_groups = {
        'uke_waza_123':
            {'name':'Uke Waza 1-3',
             'description': "First set of the blocking drills.  Common feature: No stepping.",
             'skillslugs': ['uke_waza_1', 'uke_waza_2', 'uke_waza_3']
            },
        'yotsukado_01_03':
            {'name': 'Yotsukado No Renshu 1-3',
             'description': "First set of the 4-corner drills.  Common feature: all stances are in Shizentai Dachi.",
             'skillslugs': ['yotsukado_01', 'yotsukado_02', 'yotsukado_03']
            },
        'yotsukado_04_06':
            {'name': 'Yotsukado No Renshu 4-6',
             'description': "Second set of the 4-corner drills.  Common feature: all stances are in Zenkutsu Dachi.",
             'skillslugs': ['yotsukado_04', 'yotsukado_05', 'yotsukado_06']
             },
        'yotsukado_07_09':
            {'name': 'Yotsukado No Renshu 7-9',
             'description': "Third set of the 4-corner drills.  Common feature: all stances are in Kokutsu Dachi \
             for the blocks, and transition to Zenkutsu Dachi for the punch.",
             'skillslugs': ['yotsukado_07', 'yotsukado_08', 'yotsukado_09']
             },
        'yotsukado_10_12':
            {'name': 'Yotsukado No Renshu 10-12',
             'description': "Fourth set of the 4-corner drills.  Common feature: all stances are in Neko Ashi Dachi.",
             'skillslugs': ['yotsukado_10', 'yotsukado_11', 'yotsukado_12']
             },
        'yotsukado_13_16':
            {'name': 'Yotsukado No Renshu 13-16',
             'description': "Fifth set of the 4-corner drills.",
             'skillslugs': ['yotsukado_13', 'yotsukado_14', 'yotsukado_15', 'yotsukado_16']
             },
        'yotsukado_17_20':
            {'name': 'Yotsukado No Renshu 17-20',
             'description': "Last set of the 4-corner drills.",
             'skillslugs': ['yotsukado_17', 'yotsukado_18', 'yotsukado_19', 'yotsukado_20']
             },
        'shugoro_yak_2_4':
            {'name': 'Shugoro Yakusoku Kumite 2-4',
             'description': "The three shortest Yakusoku Kumite drills from the Shugoro series.",
             'skillslugs': ['shugoro_yak_2', 'shugoro_yak_3', 'shugoro_yak_4']
            },
        'kobudo_1_of':
            {'name': 'One Kobudo Formal Kata',
             'description': 'Know one of the listed kobudo formal kata',
             'min_in_group': 1,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
            },
        'kobudo_2_of':
            {'name': 'Two Kobudo Formal Kata',
             'description': 'Know two of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 2,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_3_of':
            {'name': 'Three Kobudo Formal Kata',
             'description': 'Know three of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 3,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_4_of':
            {'name': 'Four Kobudo Formal Kata',
             'description': 'Know four of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 4,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_5_of':
            {'name': 'Five Kobudo Formal Kata',
             'description': 'Know five of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 5,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_6_of':
            {'name': 'Six Kobudo Formal Kata',
             'description': 'Know six of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 6,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_7_of':
            {'name': 'Seven Kobudo Formal Kata',
             'description': 'Know seven of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 7,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_8_of':
            {'name': 'Eight Kobudo Formal Kata',
             'description': 'Know eight of the listed kobudo formal kata (One new from previous rank)',
             'min_in_group': 8,
             'skillslugs': ['shushi_1', 'kubo', 'sakugawa_1', 'nunchaku_1', 'tonfa_1', 'sai_1', 'eku_1', 'kama_1'],
             },
        'kobudo_kumite_1':
            {'name': 'One Kobudo Kumite',
             'description': 'Know one of the listed kobudo kumite drills',
             'min_in_group': 1,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_2':
            {'name': 'Two Kobudo Kumite',
             'description': 'Know two of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 2,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_3':
            {'name': 'Three Kobudo Kumite',
             'description': 'Know three of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 3,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_4':
            {'name': 'Four Kobudo Kumite',
             'description': 'Know four of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 4,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_5':
            {'name': 'Five Kobudo Kumite',
             'description': 'Know five of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 5,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_6':
            {'name': 'Six Kobudo Kumite',
             'description': 'Know six of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 6,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_7':
            {'name': 'Seven Kobudo Kumite',
             'description': 'Know seven of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 7,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
        'kobudo_kumite_8':
            {'name': 'Eight Kobudo Kumite',
             'description': 'Know eight of the listed kobudo kumite drills (One new from previous rank)',
             'min_in_group': 8,
             'skillslugs': ['bo_sai_1', 'bo_sai_2', 'bo_sai_3',
                            'bo_kama_1', 'bo_kama_2', 'bo_kama_3',
                            'bo_tonfa_1', 'bo_tonfa_2', 'bo_tonfa_3'],
             },
    }

    for slug, details in skill_groups.items():
        subskills = []
        skillslugs = details.pop('skillslugs')
        for skillslug in skillslugs:
            subskills.append(skills[skillslug])
        details['group'] = subskills
        if 'min_in_group' in details:
            skills[slug] = style.N_OfRequirementGroup(slug=slug, **details)
        else:
            skills[slug] = style.RequirementGroup(slug=slug, **details)
        db.add(skills[slug])

    curriculum_requirements = {}

    curriculum_requirements['karate'] = {
        'white': {
            'red': 'kihon_uke',
            'yellow': 'kihon_geri',
            'blue': 'kihon_dachi',
            'green': 'kihon_uchi',
            'class_time': 16,
            'other': ['Patches sewn on gi', 'Can tie own belt correctly']
        },
        'white-black': {
            'red': 'kihon_1',
            'yellow': 'kihon_2',
            'blue': 'kihon_3',
            'green': 'fukyu_1',
            'class_time': 16,
        },
        'gold': {
            'red': 'fukyu_2',
            'yellow': 'empi_waza',
            'blue': 'uke_waza_123',
            'green': 'taikyoku_1',
            'class_time': 16,
            'other': 'Sparring gear'
        },
        'gold-black': {
            'red': 'nihanchi_1',
            'yellow': 'fukyu_kata',
            'blue': 'yotsukado_01_03',
            'green': 'taikyoku_2',
            'class_time': 20,
        },
        'orange': {
            'red': 'nihanchi_2',
            'yellow': 'shuto_waza',
            'blue': 'bo_taiso',
            'green': 'taikyoku_3',
            'class_time': 20,
            'other': 'Owns Bo before Blue stripe'
        },
        'orange-black': {
            'red': 'nihanchi_3',
            'yellow': 'uke_waza_4',
            'blue': 'yotsukado_04_06',
            'green': 'taikyoku_4',
            'class_time': 20,
        },
        'purple': {
            'red': 'pinan_1',
            'yellow': 'minoru_yak_1',
            'blue': 'nunchaku_taiso',
            'green': 'taikyoku_5',
            'class_time': 40,
            'other': 'Owns Nunchaku before Blue stripe'
        },
        'purple-black': {
            'red': 'pinan_2',
            'yellow': 'minoru_yak_2',
            'blue': 'yotsukado_07_09',
            'green': 'minoru_yak_3',
            'class_time': 40,
        },
        'blue': {
            'red': 'pinan_3',
            'yellow': 'minoru_yak_4',
            'blue': 'tonfa_taiso',
            'green': 'minoru_yak_5',
            'class_time': 40,
            'other': 'Owns Tonfa before Blue stripe'
        },
        'blue-black': {
            'red': 'pinan_4',
            'yellow': 'minoru_yak_6',
            'blue': 'yotsukado_10_12',
            'green': 'minoru_yak_7',
            'class_time': 40,
        },
        'green': {
            'red': 'pinan_5',
            'yellow': 'shugoro_yak_1',
            'blue': 'sai_taiso',
            'green': 'shugoro_yak_2_4',
            'class_time': 60,
            'teach_time': 20,
            'other': 'Owns Sai before Blue stripe'
        },
        'green-black': {
            'red': 'passai_sho',
            'yellow': 'shugoro_yak_5',
            'blue': 'yotsukado_13_16',
            'green': 'shugoro_yak_6',
            'teach_time': 20,
            'class_time': 90,
        },
        'brown': {
            'red': 'passai_dai',
            'yellow': 'kusanku_sho',
            'blue': 'eku_taiso',
            'green': 'shugoro_yak_7',
            'class_time': 120,
            'teach_time': 20,
            'other': 'Owns Eku before Blue stripe'
        },
        'brown-black': {
            'red': 'chinto',
            'yellow': 'gorin',
            'blue': 'yotsukado_17_20',
            'class_time': 200,
            'teach_time': 50,
            'other': ['Attend all dojo events','1 year min as Brown/Black']
        },
        'shodan': {
            'red': 'kusanku_dai',
            'blue': 'kama_taiso',
            'class_time': 300,
            'teach_time': 100,
            'other': 'Owns Kama before Blue stripe'
        },
        'nidan': {
            'red': 'gojushiho',
            'class_time': 400,
            'teach_time': 200,
        },
        'sandan': {
            'class_time': 500,
            'teach_time': 300,
        },

    }

    curriculum_requirements['kobudo'] = {
        'white': {
            'red': 'bo_taiso',
            'class_time': 6,
        },
        'white-black': {
            'red': 'nunchaku_taiso',
            'yellow': 'bo_bo_1',
            'class_time': 9,
        },
        'gold': {
            'red': 'tonfa_taiso',
            'yellow': 'bo_bo_2',
            'class_time': 9,
        },
        'gold-black': {
            'red': 'sai_taiso',
            'yellow': 'bo_bo_3',
            'class_time': 9,
        },
        'orange': {
            'red': 'eku_taiso',
            'yellow': 'bo_bo_4',
            'class_time': 9,
        },
        'orange-black': {
            'red': 'kama_taiso',
            'yellow': 'bo_bo_5',
            'class_time': 9,
        },
        'purple': {
            'red': 'kobudo_1_of',
            'yellow': 'kobudo_kumite_1',
            'class_time': 15,
        },
        'purple-black': {
            'red': 'kobudo_2_of',
            'yellow': 'kobudo_kumite_2',
            'class_time': 15,
        },
        'blue': {
            'red': 'kobudo_3_of',
            'yellow': 'kobudo_kumite_3',
            'class_time': 15,
        },
        'blue-black': {
            'red': 'kobudo_4_of',
            'yellow': 'kobudo_kumite_4',
            'class_time': 15,
        },
        'green': {
            'red': 'kobudo_5_of',
            'yellow': 'kobudo_kumite_5',
            'class_time': 24,
        },
        'green-black': {
            'red': 'kobudo_6_of',
            'yellow': 'kobudo_kumite_6',
            'class_time': 24,
        },
        'brown': {
            'red': 'kobudo_7_of',
            'yellow': 'kobudo_kumite_7',
            'class_time': 24,
        },
        'brown-black': {
            'red': 'kobudo_8_of',
            'yellow': 'kobudo_kumite_8',
            'class_time': 48,
        },
    }

    for cur_slug, cur_spec in curriculum_requirements.items():
        for rank_slug, rank_spec in cur_spec.items():
            for spec_tag, req_slugs in rank_spec.items():
                if spec_tag in ['red', 'yellow', 'blue', 'green']:
                    if not isinstance(req_slugs, list):
                        req_slugs = [req_slugs]
                    for req_slug in req_slugs:
                        req = skills[req_slug]
                        rank_req = style.RankRequirement(
                            _from=c_ranks[cur_slug][rank_slug]._id,
                            _to=req._id,
                            tag=spec_tag
                        )
                        db.add(rank_req)
                elif spec_tag == 'class_time':
                    spec = {
                        'name': str(req_slugs) + " Hours",
                        'slug': "_".join([cur_slug, rank_slug, spec_tag]),
                        'description': f'Student must have at least {req_slugs} hours of time in class to be eligible \
                        for promotion to the next rank.',
                    }
                    req = style.TimeInClassRequirement(**spec)
                    req.min_hours(req_slugs)
                    db.add(req)
                    rank_req = style.RankRequirement(
                        _from=c_ranks[cur_slug][rank_slug]._id,
                        _to=req._id,
                        tag=spec_tag
                    )
                    db.add(rank_req)
                elif spec_tag == 'teach_time':
                    spec = {
                        'name': str(req_slugs) + " Hours",
                        'slug': "_".join([cur_slug, rank_slug, spec_tag]),
                        'description': f'Student must have at least {req_slugs} hours of time in the role of \
                         instructor or assistant instructor to be eligible for promotion to the next rank.',
                    }
                    req = style.TimeInClassRequirement(**spec)
                    req.min_hours(req_slugs)
                    db.add(req)
                    rank_req = style.RankRequirement(
                        _from=c_ranks[cur_slug][rank_slug]._id,
                        _to=req._id,
                        tag=spec_tag
                    )
                    db.add(rank_req)
                elif spec_tag == 'other':
                    if not isinstance(req_slugs, list):
                        req_slugs = [req_slugs]
                    for req_slug in req_slugs:
                        req = style.GenericRequirement(name=req_slug)
                        db.add(req)
                        rank_req = style.RankRequirement(
                            _from=c_ranks[cur_slug][rank_slug]._id,
                            _to=req._id,
                            tag=spec_tag
                        )
                        db.add(rank_req)

    people_dict = {
        'Joël': {
            'title': 'Sensei',
            'first_name': 'Joël',
            'middle_name': 'Paul',
            'last_name': 'Larose',
            'date_of_birth': dt.date(1977,11,12),
            'phones': {
                'cell': {
                    'value': '519-897-9980',
                    'can_text': True,
                }
            },
            'emails': {
                'personal': 'joel.larose@gmail.com',
                'work': 'sensei-joel@larosekarate.com',
            },
        },
        'Anthony': {
            'first_name': 'Anthony',
            'last_name': 'Moore',
            'date_of_birth': dt.date(1980, 11,13),
            'phones': {
                'cell': '519-500-6367',
            },
            'emails': {
                'personal': 'anthonythepianist@gmail.com',
                'work1': 'amoore@ugcloud.ca',
                'dojo': 'anthony@larosekarate.com',
            },
        },
    }

    person_objects = {}
    for name, person in people_dict.items():
        person_objects[name] = people.Person(**person)
        db.add(person_objects[name])

    households = {
        'Larose-Moore': {
            'addresses': {
                'home': {
                    'street': '82 West Acres Cres.',
                    'city': 'Kitchener',
                    'province': 'ON',
                    'postal_code': 'N2L 3G8',

                },
            },
        },
    }

    household_objects = {}
    for name, household in households.items():
        household_objects[name] = people.Household(**household)
        db.add(household_objects[name])

    family_members = {
        'Larose-Moore': [
            'Joël',
            'Anthony',
        ],
    }

    for family, members in family_members.items():
        for member in members:
            fm = people.FamilyMember(_from=person_objects[member]._id,
                                     _to=household_objects[family]._id)
            db.add(fm)


if __name__ == '__main__':
    main()