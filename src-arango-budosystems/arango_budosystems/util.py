from __future__ import annotations
from typing import *

import importlib

__all__ = ['class_for_name', 'query_by_key_or_slug', 'ParallelFactory']


def class_for_name(class_name: str, module_name: str) -> Type:
    """

    :param class_name:
    :param module_name:
    :return:
    """
    # load the module, will raise ImportError if module cannot be loaded
    m = importlib.import_module(module_name)
    # get the class, will raise AttributeError if class cannot be found
    c = getattr(m, class_name)
    return c


def query_by_key_or_slug(db, collection, keyslug):
    """

    :param db:
    :param collection:
    :param keyslug:
    :return:
    """
    doc = db.query(collection).\
        filter('_key==@keyslug', keyslug=keyslug).\
        filter('slug==@keyslug', keyslug=keyslug, _or=True).\
        first()

    return doc


SourceBase = TypeVar('SourceBase')
ParaBase = TypeVar('ParaBase')


class ParallelFactory(Generic[SourceBase, ParaBase]):
    """
    Base class for factories a parallel set of classes for the Entity class tree.

    Usage
    =====
    Subclass `ParallelFactory` providing types for the SourceBase and ParaBase generic parameters, and either prefix
    or suffix (or both) as named arguments.

    Example
    =======
    class Fs(ParallelFactory[O, F], suffix='F'):
        pass

    where O is the root of the original class tree, and F is the root of the class tree paralleling the original tree

    Fs(SubO) will look for a class called SubOF, and if it doesn't exist then create it.
    Returns the result of the search or creation.

    Class Parameters
    ================
    The following arguments are allowed during class definition of a subclass of EntityParallelFactory:
    SourceBase
        Base class for the set of classes which will be paralleled with this factory.  (Required)
    ParaBase
        Base class for the parallel set of classes generated by this factory class.  (Required)
    prefix
        Common prefix for all subclasses of para_base. (Default: '')
    suffix
        Common suffix for all subclasses of para_base. (Default: '')

    .. note::
        At least one of prefix or suffix must be provided as a non-empty string.  These are used both for
        retrieving and creating subclasses.
    """

    _parallel_classes = dict()

    @classmethod
    def __init_subclass__(cls, prefix: str = "", suffix: str = "", **kwargs) -> None:
        """

        :param prefix:
        :param suffix:
        :param kwargs:
        """
        super().__init_subclass__()

        # Get Generic parameter values
        gen_args = cls.__orig_bases__[0].__args__
        # print(f'{cls.__name__}.gen_args = {gen_args}')
        source_base = gen_args[0]
        para_base = gen_args[1]

        if source_base is None:
            raise TypeError("SourceBase must not be None")
        if para_base is None:
            raise TypeError("ParaBase must not be None")
        if not isinstance(source_base, type):
            raise TypeError("SourceBase must be a type")
        if not isinstance(para_base, type):
            raise TypeError("ParaBase must be a type")
        if prefix is None:
            prefix = ""
        if suffix is None:
            suffix = ""
        if prefix + suffix == "":
            raise AttributeError("At least one of 'prefix' or 'suffix' must be specified with a non-empty string.")
        cls.source_base_class = source_base
        cls.parallel_base_class = para_base
        cls.parallel_prefix = prefix
        cls.parallel_suffix = suffix

    @classmethod
    def register(cls, parallel_class: Type[ParaBase]) -> None:
        """Adds the `parallel_class` to inventory of known classes.
        :param parallel_class:
        """
        if not issubclass(parallel_class, cls.parallel_base_class):
            raise TypeError(f"parallel_class must be a subclass of {str(cls.parallel_base_class)}")
        cls._parallel_classes[parallel_class.__name__] = parallel_class

    def __new__(cls, data_class: Type[SourceBase], *args, **kwargs) -> Type[ParaBase]:
        """

        :param data_class:
        :param args:
        :param kwargs:
        """
        if data_class is None:
            raise ValueError('data_class required')
        if not issubclass(data_class, cls.source_base_class):
            raise TypeError(f'data_class must be a subclass of {str(cls.parallel_base_class)}, got {str(data_class)}')

        p_name = cls.parallel_prefix + data_class.__name__ + cls.parallel_suffix
        if p_name in cls._parallel_classes:
            parallel_class = cls._parallel_classes[p_name]
        else:
            parallel_class = cls.build_parallel_class(data_class, p_name)
        return parallel_class

    @classmethod
    def build_parallel_class(cls, data_class: Type[SourceBase], p_name: str, extra_attrs: Mapping[str, Any] = {}) -> Type[ParaBase]:
        """

        :param data_class:
        :param p_name:
        :param extra_attrs:
        :return:
        """
        parallel_attrs = {'data_class': data_class, **extra_attrs}

        # generated class assumes a parallel hierarchy
        if data_class == cls.source_base_class:  # recursion stopper
            super_p_class = (cls.parallel_base_class,)
        elif issubclass(data_class, cls.source_base_class):
            super_p_class = []
            for b in data_class.__bases__:
                if issubclass(b, cls.source_base_class):
                    super_p_class.append(cls(b))
        else:
            raise TypeError()

        parallel_class = type(p_name, tuple(super_p_class), parallel_attrs)

        return parallel_class

