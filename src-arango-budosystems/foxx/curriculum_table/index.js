'use strict';
const db = require('@arangodb').db;
const aql = require('@arangodb').aql;
const createRouter = require('@arangodb/foxx/router');
const router = createRouter();
const joi = require('joi');

module.context.use(router);

/*
const colls = {
    curricula: db._collection('curricula'),
    ranks: db._collection('ranks'),
    styles: db._collection('styles'),
};
*/

function queryByKeyOrSlug(coll, keyslug){
    var q = aql`
        FOR doc IN ${coll}
            FILTER doc._key == ${keyslug} OR doc.slug == ${keyslug}
            RETURN doc
    `;

    return db._query(q).toArray();
}

function curriculumTable(request, response){
    var currName = request.pathParams.curriculum;
    var currDocs = queryByKeyOrSlug(db.curricula, currName);

    if (currDocs.length == 0) {
        response.throw(404, 'Curriculum not found.');
        return;
    }
    var data = [];
    for (var i = 0; i < currDocs.length; i++) {
        var currDoc = currDocs[i];
        var currRanks = db.curriculum_for_rank.outEdges(currDoc);

        var table = {};
        var cr, rank, rankReqs, rreq, req;

        for (var j = 0; j < currRanks.length; j++) {
            cr = currRanks[j];
            rank = db._document(cr._to);
            table[rank.slug] = {};
            rankReqs = db.rank_requirements.outEdges(cr);
            for (var k = 0; k < rankReqs.length; k++) {
                rreq = rankReqs[k];
                req = db._document(rreq._to);
                if (table[rank.slug][rreq.tag]) {
                    table[rank.slug][rreq.tag].push(req);
                }
                else {
                    table[rank.slug][rreq.tag] = [req];
                }
            }
        }
        data.push({
            "document": currDoc,
            "table": table
        });
    }
    response.send(data);
}

router.get('/curriculum/:curriculum', curriculumTable)
    .pathParam('curriculum', joi.string().required(), 'slug or key for the curriculum to be returned in table form')
    .response(['application/json'], 'A JSON formatted list of requirements for the curriculum.')
    .summary('List of requirements for the given curriculum.')
    .description("Produces a JSON formatted list of requirements for the given curriculum. "
        + "The nested structure represents a table format such that "
        + "one axis (typically the rows) represents the ranks, "
        + "the other axis (typically the columns) represents the tags associated with each requirement, "
        + "and the inner cells list the requirements for the rank and tag of the corresponding axes.")
    ;