import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
#with open(os.path.join(here, 'README.txt')) as f:
#    README = f.read()
#with open(os.path.join(here, 'CHANGES.txt')) as f:
#    CHANGES = f.read()

requires = [
    'python-arango',
    'arango-orm',
    'arango',
    'marshmallow',
]

tests_require = [
    'pytest >= 3.7.4',
    'pytest-cov',
]

dev_requires = [
]

setup(
    name='arango_budosystems',
    version='0.0',
    description='arango_budosystems',
#    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='',
    author_email='',
    url='',
    keywords='web arango martialarts',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
        'dev': dev_requires,
    },
    install_requires=requires,
)
