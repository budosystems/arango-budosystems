from __future__ import annotations
from typing import *

from pyramid.view import view_defaults
from pyramid.httpexceptions import *

import inspect
import importlib

from arango_budosystems import model
from arango_budosystems.model import *
from arango_budosystems.util import ParallelFactory

from pyramid_budosystems.views import t
from pyramid_budosystems.forms.core import EntityForms


class EntityPath:
    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.db = request.registry.db

    def listing(self, data_obj):
        self.request.route_url(data_obj.classname())

    def item(self, data_obj):
        return self.request.route_url(data_obj.classname()+'key', keyslug=data_obj.keyslug)

    def add(self, data_obj):
        self.request.route_url(data_obj.classname()+'add')

    def edit(self, data_obj):
        return self.request.route_url(data_obj.classname()+'edit', keyslug=data_obj.keyslug)

    def delete(self, data_obj):
        return self.request.route_url(data_obj.classname()+'delete', keyslug=data_obj.keyslug)


class BudoView():
    """Base class for Views."""
    pass


class EntityViews(ParallelFactory[core.Entity, BudoView], suffix="View"):
    """Factory class for model views."""
    pass


class EntityViewMeta(type):
    """Meta class for model views."""
    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        EntityViews.register(cls)


@view_defaults(renderer='json')
class EntityView(BudoView, metaclass=EntityViewMeta):
    """Base class for model views."""
    data_class = core.Entity

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.db = request.registry.db
        self.path_gen = EntityPath(context, request)
#        print("EntityView.__init__: view_class = " + str(self.data_class))

    def _base_values(self, plural: bool=False) -> Dict[str, Any]:
        """
        Assembles and returns the base values to be sent to the template engine.

        :param plural: whether or not to look for the plural form of the entity name.

        :return: A dict of the basic data shared by all the view methods.
        """

        titletype = self.data_class.classname()
        if hasattr(self.data_class, 'Meta'):
            if plural:
                if hasattr(self.data_class.Meta, 'verbose_name_plural'):
                    titletype = self.data_class.Meta.verbose_name_plural
                elif hasattr(self.data_class.Meta, 'verbose_name'):
                    titletype = self.data_class.Meta.verbose_name + "s"
                else:
                    titletype += "s"
            elif hasattr(self.data_class.Meta, 'verbose_name'):
                titletype = self.data_class.Meta.verbose_name

        values = {'entity_class': self.data_class,
                  'titletype': f"{titletype.title()}",
                  'fields': self.data_class._fields,
                  'show_nones': False,
                  'show_extras': True,
                  'exclude': ['_entity_type', '_key'],
                  'path_gen': self.path_gen
                  }
        return values

    def list_all(self):
        """

        :return:
        """
        if not self.db.has_collection(self.data_class):
            return None
        return self.db.query(self.data_class).all()

    def list_template(self):
        """

        :return:
        """
        values = self._base_values(plural=True)

        values.update({
            'entities': self.list_all(),
            'headertitle': 'List of All ' + values['titletype'],
        })
        return values

    def get_item(self, keyslug: str) -> core.Entity:
        """

        :param keyslug:

        :return:
        """
        if not self.db.has_collection(self.data_class):
            return None
        condition = f'$REC._key==@keyslug OR $REC.slug==@keyslug'
        res = self.db.query(self.data_class).filter(condition,
                                                    prepend_rec_name=False,
                                                    rec_name_placeholder="$REC",
                                                    keyslug=keyslug).first()
        return res

    def item_template(self):
        """

        :return:
        """
        keyslug = self.request.matchdict['keyslug']
        item = self.get_item(keyslug)

        if not item:
            raise HTTPNotFound()

        values = self._base_values()
        values.update({
            'ent': item,
            'headertitle': f"{values['titletype']} ({item._key})",
        })

        return values

    def add_form(self):
        """

        :return:
        """
        form = EntityForms[self.data_class](formdata=self.request.POST)
        if self.request.method == 'POST' and form.validate():
            item = self.data_class()
            form.populate_obj(item)
            self.db.add(item)
            fwd_to = self.path_gen.item(item)
            raise HTTPCreated(headers=dict(Location=fwd_to, Refresh=f'0; URL={fwd_to}'))

        values = self._base_values()
        values.update({
            'show_nones': True,
            'headertitle': f"{values['titletype']} (New)",
            'form': form,
            'errors': form.errors,
        })
        return values

    def edit_form(self):
        """

        :return:
        """
        keyslug = self.request.matchdict['keyslug']
        item = self.get_item(keyslug)

        if not item:
            raise HTTPNotFound()

        form = EntityForms[self.data_class](formdata=self.request.POST, obj=item)
        if self.request.method == 'POST' and form.validate():
            form.populate_obj(item)
            self.db.update(item)
            #fwd_to = f"{self.data_class.__collection__}/{item._key}"
            raise HTTPResetContent()

        values = self._base_values()
        values.update({
            'ent': item,
            'headertitle': f"{values['titletype']} ({item._key})",
            'form': form,
            'errors': form.errors,
        })

        return values

    def delete_form(self):
        """

        """
        raise HTTPNotImplemented()


class PersonView(EntityView):
    """View for the Person entity type."""
    data_class = people.Person

    def list_template(self):
        """

        :return:
        """
        values = super().list_template()
        values['show_nones'] = True
        return values


def includeme(config):
    """

    :param config:
    """
    # Model routes
    # get all the modules under arango_budosystems.model
    for mdl_name, mdl in inspect.getmembers(model, lambda x: inspect.ismodule(x)):
        # get all the classes in the module
        for cls_name, cls in inspect.getmembers(mdl, lambda x: inspect.isclass(x)):
            if issubclass(cls, core.Entity) and cls.collection():
                # create a new view class using generics syntax
                viewcls = EntityViews(cls)
                classname = cls.classname()
                collection = cls.collection()

                config.add_route(classname, '/'+collection)
                config.add_route(classname+'add', '/'+collection+'/add')
                config.add_route(classname+'key', '/'+collection+'/{keyslug}')
                config.add_route(classname+'edit', '/'+collection+'/{keyslug}/edit')
                config.add_route(classname+'delete', '/'+collection+'/{keyslug}/delete')

                config.add_view(view=viewcls,
                                attr='list_template',
                                route_name=classname,
                                renderer=t('entity_list')
                                )
                config.add_view(view=viewcls,
                                attr='add_form',
                                route_name=classname+'add',
                                renderer=t('entity_admin_page')
                                )
                config.add_view(view=viewcls,
                                attr='item_template',
                                route_name=classname+'key',
                                renderer=t('entity_page')
                                )
                config.add_view(view=viewcls,
                                attr='edit_form',
                                route_name=classname+'edit',
                                renderer=t('entity_admin_page')
                                )
                config.add_view(view=viewcls,
                                attr='delete_form',
                                route_name=classname+'delete',
                                renderer=t('entity_admin_page')
                                )

                with config.route_prefix_context('/json'):
                    config.add_route('json'+classname, '/'+collection)
                    config.add_route('json'+classname+'key', '/'+collection+'/{keyslug}')
                    config.add_view(view=viewcls, attr='list_all', route_name='json'+cls.__name__)
                    config.add_view(view=viewcls, attr='get_item', route_name='json'+cls.__name__+'key')

    #config.add_exception_view()

