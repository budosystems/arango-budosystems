from __future__ import annotations
from typing import *

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPNotFound

from arango_budosystems.util import query_by_key_or_slug
from arango_budosystems.model import style
from pyramid_budosystems.views import t


class CurriculumTableView:
    """View for a Curriculum formatted as a table."""
    def __init__(self, context, request):
        """

        :param context:
        :param request:
        """
        self.context = context
        self.request = request
        self.db = request.registry.db
        self.curriculum = request.matchdict['keyslug']

    def _crosstab(self):
        """

        :return:
        """
        curr_doc = query_by_key_or_slug(self.db, style.Curriculum, self.curriculum)
        if curr_doc is None:
            raise HTTPNotFound

        # curr_ranks = graph.edge_collection('curriculum_for_rank').edges(curr_doc._id, 'out')['edges']
        curriculum_ranks = curr_doc.links(style.CurriculumRank, 'out')
        table = {}

        for cr in curriculum_ranks:
            rank = cr.to_entity
            table[rank.slug] = {}
            rank_reqs = cr.links(style.RankRequirement, 'out')
            for rank_req in rank_reqs:
                req = rank_req.to_entity
                if rank_req.tag not in table[rank.slug]:
                    table[rank.slug][rank_req.tag] = []
                table[rank.slug][rank_req.tag].append(req)

        return curr_doc, table

    def table(self):
        """

        :return:
        """
        #try:
        curr_doc, table = self._crosstab()
        #except:
        #    raise HTTPNotFound()

        ranks = curr_doc.ranks
        ranks.reverse()

        values = {
            "curriculum": curr_doc,
            "curr_table": table,
            "ranks": ranks,
        }
        return values


def includeme(config):
    """

    :param config:
    """
    config.add_route('curriculum_table', 'curriculum_table/{keyslug}')
    config.add_view(view=CurriculumTableView,
                    attr='table',
                    route_name='curriculum_table',
                    renderer=t('curriculum_table')
                    )