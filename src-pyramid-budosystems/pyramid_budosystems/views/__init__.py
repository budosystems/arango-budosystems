from __future__ import annotations
from typing import *

def t(template_name: str, package_name: str='pyramid_budosystems', path: str='templates', ext: str='jinja2') -> str:
    """
    Assembles the template path for a given template name.

    Defaults are specific to this application.

    :param template_name:
    :param package_name:
    :param path:
    :param ext:
    """
    ret = ""
    if package_name:
        ret += package_name + ":"
    if path:
        ret += path + "/"

    ret += template_name
    if ext:
        ret += "." + ext
    return ret
