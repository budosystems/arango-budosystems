from __future__ import annotations
from typing import *

from pyramid.config import Configurator
from pyramid.renderers import JSON
from arango_budosystems.db import connect_from_settings
from arango_budosystems.model.core import Entity


def entity_json_adapter(obj, request):
    """
    :param obj:
    :param request:
    """
    return obj._dump()


def main(global_config, **settings):
    """This function returns a Pyramid WSGI application.

    :param global_config:
    :param **settings:
    """
    db = connect_from_settings(settings)
    json_renderer = JSON()
    json_renderer.add_adapter(Entity, entity_json_adapter)


    with Configurator(settings=settings) as config:
        config.registry.db = db
        config.add_renderer('json', json_renderer)

        config.include('pyramid_jinja2')
        config.include('.routes')
        config.include('.views.modelviews')
        config.include('.views.curriculum_table_view')
        config.scan()

    return config.make_wsgi_app()


