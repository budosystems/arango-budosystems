from __future__ import annotations
from typing import *

from wtforms.form import Form as WTForm
from wtforms.form import FormMeta
from wtforms import fields as wtfields
from wtforms.fields import html5 as wtfields5
from wtforms import validators

from arango_orm import fields as aofields

from arango_budosystems.util import ParallelFactory
from arango_budosystems.model import *

from wtforms.compat import with_metaclass


#: Default mapping from arango_orm.fields to wtforms.fields
defaults = {
    aofields.Boolean:           wtfields.BooleanField,
    aofields.Constant:          wtfields.HiddenField,
    aofields.Date:              wtfields5.DateField,
    aofields.DateTime:          wtfields5.DateTimeField,
    aofields.Decimal:           wtfields5.DecimalField,
    aofields.Dict:              wtfields.FieldList,
    aofields.Email:             wtfields5.EmailField,
    aofields.Float:             wtfields5.DecimalField,
    aofields.FormattedString:   wtfields.TextField, # Check formatting?
    aofields.Integer:           wtfields5.IntegerField,
    aofields.List:              wtfields.FieldList,
    aofields.Nested:            wtfields.FormField,
    aofields.Number:            wtfields5.IntegerField,
    aofields.Raw:               wtfields.TextField,
    aofields.LocalDateTime:     wtfields5.DateTimeLocalField,
    aofields.String:            wtfields.StringField,
    aofields.Time:              wtfields5.TimeField,
    aofields.TimeDelta:         wtfields5.TimeField,
    aofields.Url:               wtfields5.URLField,
    aofields.UUID:              wtfields.StringField,
}


class BudoForm(WTForm):
    """Generic base class for forms in Budo Systems."""

    def __init__(self, context=None, request=None, *args, **kwargs):
        """
        Args:
            context:
            request:
            *args:
            **kwargs:
        """
        super().__init__(*args, **kwargs)
        self.context = context
        self.request = request
        try:
            self.db = request.registry.db
        except:
            self.db = None


class EntityForms(ParallelFactory[core.Entity, BudoForm], suffix="Form"):
    """Factory class for forms related to arango_budosystems.model classes."""
    #parallel_type = "Form"
    #parallel_base_class = BudoForm

    @classmethod
    def build_parallel_class(cls, data_class, form_name, extra_attrs={}):
        """
        Args:
            data_class:
            form_name:
            extra_attrs:
        """
        formcls = super().build_parallel_class(data_class, form_name, extra_attrs)

        for fieldname, field in data_class._fields.items():
            aofcls = type(field)
            if isinstance(field, aofields.Field) and not hasattr(formcls, fieldname):
                if aofcls in defaults:
                    wtfcls = defaults[aofcls]
                    field_attr = {'validators': [], 'render_kw': {}}

                    if issubclass(aofcls, aofields.Nested):
                        field_attr['form_class'] = EntityForms(field.nested.data_class)
                    elif issubclass(aofcls, (aofields.Dict, aofields.List)):
                        field_attr['unbound_field'] = wtfields.Field()

                    if field.required:
                        field_attr['validators'].append(validators.InputRequired())


                    form_attr = wtfcls(**field_attr)
                    setattr(formcls, fieldname, form_attr)

        return formcls


class EntityFormMeta(FormMeta):
    """
    Meta class for EntityForm classes.

    Auto-registers the Form class in the EntityForms factory.
    """

    def __init__(self, *args, **kwargs):
        """

        :param *args:
        :param **kwargs:
        """
        super().__init__(*args, **kwargs)
        EntityForms.register(self)


class EntityForm(with_metaclass(EntityFormMeta, BudoForm)):
    """Base class for forms related to arango_budosystems.model classes."""

    data_class = core.Entity

    entity_key = wtfields.HiddenField()

    def populate_obj(self, obj: core.Entity):
        """

        :param obj:
        """
        self._key = self.entity_key
        del self.entity_key
        super().populate_obj(obj)


class BasicInfoForm(EntityForm):
    """Adds the fields corresponding to core.BasicInfo."""

    data_class = core.BasicInfo

    name = wtfields.StringField(validators=[validators.InputRequired()])
    description = wtfields.TextAreaField(render_kw=dict(cols=40, rows=4))
    slug = wtfields.StringField(validators=[validators.InputRequired()])
