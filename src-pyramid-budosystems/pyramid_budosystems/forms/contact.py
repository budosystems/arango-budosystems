from __future__ import annotations
from typing import *

from pyramid_budosystems.forms import core as fcore
import arango_budosystems.model as m
from wtforms import fields as wtfields
from wtforms.fields import html5 as wtfields5


class ContactForm(fcore.EntityForm):
    """
    Form representing a Contact entry.
    """
    data_class = m.contact.Contact

    is_preferred = wtfields.BooleanField(label="Preferred?")
    label_field = wtfields.StringField(label='Label', render_kw={'placeholder':'Label (e.g. "home", "work")'})
    value_field = wtfields.StringField(label='Value', render_kw={'placeholder':'Value'})


class PhysicalAddressForm(ContactForm):
    data_class = m.contact.PhysicalAddress

    value_field = wtfields.HiddenField()
    street = wtfields.TextAreaField()
    city = wtfields.StringField()
    province = wtfields.StringField()
    postal_code = wtfields.StringField()
    country = wtfields.StringField(default='CA')


class PhoneNumberForm(ContactForm):
    data_class = m.contact.PhoneNumber

    value_field = wtfields5.TelField(label='Phone Number', render_kw={'placeholder':'Phone Number'})
    can_text = wtfields.BooleanField()
    is_fax = wtfields.BooleanField()


class EmailAddressForm(ContactForm):
    data_class = m.contact.EmailAddress

    value_field = wtfields5.EmailField(label='Email Address', render_kw={'placeholder':'you@example.com'})


class ContactListForm(fcore.EntityForm):
    data_class = m.contact.ContactList

    field_list = wtfields.FieldList(wtfields.FormField(ContactForm), min_entries=1)
    preferred = wtfields.HiddenField()


class AddressListForm(ContactListForm):
    field_list = wtfields.FieldList(wtfields.FormField(fcore.EntityForms(m.contact.PhysicalAddress)), min_entries=1)


class PhoneListForm(ContactListForm):
    field_list = wtfields.FieldList(wtfields.FormField(PhoneNumberForm), min_entries=1)


class EmailListForm(ContactListForm):
    field_list = wtfields.FieldList(wtfields.FormField(EmailAddressForm), min_entries=1)


class ContactableForm(fcore.EntityForm):
    data_class = m.contact.Contactable

#    addresses = wtfields.FormField(AddressListForm)
#    phones = wtfields.FormField(PhoneListForm)
#    emails = wtfields.FormField(EmailListForm)
    addresses = wtfields.HiddenField()
    phones = wtfields.HiddenField()
    emails = wtfields.HiddenField()


